---
layout: post
title:  "Slides: \"Configuration Management and Salt\""
date:   2016-11-12 23:00:00 +0000
comments: true
---

Here are the slides for my talk about
<a href="https://saltstack.com/community/">Salt</a>
at <a href="http://www.bradlug.co.uk/">Bradford Linux Users Group</a>
on Monday 14th November 2016.

<iframe src="//www.slideshare.net/slideshow/embed_code/key/kZqAvQwe5sYgds"
  width="599" height="487" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen
  >
</iframe>
