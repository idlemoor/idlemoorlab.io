---
layout: post
title:  "Upgrading a Slackware instance on OpenVZ"
date:   2017-01-10 20:00:00 +0000
comments: true
---

I've just bought an OpenVZ VPS.
Good news: the provider has a Slackware template!
Bad news: it's the same ancient Slackware 13.37 template that AlienBOB
blogged about last year.  See
['Running Slackware 14.1 in an OpenVZ VPS' at Alien Pastures](http://alien.slackbook.org/blog/running-slackware-14-1-in-an-openvz-vps/).

#### Slackware 14.1

AlienBOB's Slackware 14.1 template is available on openvz.org, see the
'Contributed templates' here:
[https://openvz.org/Download/template/precreated](https://openvz.org/Download/template/precreated)

What can you do if your VPS provider hasn't made the Slackware 14.1 template available?
You can download and install more network packages from Slackware 13.37, and then you
can download and upgrade to 14.0, and then you can download and upgrade to 14.1.
Obviously this is tedious and painful.

But there is a sneaky way of directly using the Slackware 14.1 template
to upgrade a running Slackware 13.37 VPS.

  * First, *please backup everything important to another system!*

  * Now copy AlienBOB's template into your VPS. For a new Slackware 13.37 VPS,
you will have to download it to another system and then use scp to copy
it into your VPS, because the 13.37 template is extremely minimalist,
and ssh/scp is the only network tool that is installed.

  * When you have the template inside your vps, create a new directory and
extract AlienBOB's template into it.
```
mkdir /new
cd /new
tar xf ~/slackware-14.1-x86_64-minimal-20150519.tar.xz
```
  * Now you need to customise some of the files in /new/etc/ -- here is what I did,
  but please use your head and do the right thing for your own system:

  * Edit /new/etc/rc.d/rc.inet1.conf, mine is now like this:
```
IFNAME[0]="venet0"
IPADDR[0]="31.7.184.118"
NETMASK[0]="255.255.255.255"
USE_DHCP[0]=""
DHCP_HOSTNAME[0]=""
GATEWAY="192.0.2.1"
```
  * I got ipv6 working on the venet0 device, by adding this to /new/etc/rc.d/rc.local:
```
ip -6 route add ::/0 dev venet0
ip -6 addr add 2a01:4a0:25::47a0:41fc dev venet0
```
  * Preserved some more files
```
cp /etc/HOSTNAME /etc/hosts /new/etc/
cp /etc/ssh/ssh_host_* /new/etc/ssh/
cp /etc/passwd /etc/shadow /etc/group /new/etc/
```
  * Disabled system logging
```
chmod ugo-x /new/etc/rc.d/rc.syslog
```
  * It turns out that my VPS provider re-edits /etc/inittab and
  /etc/rc.d/rc.inet1 on every reboot. Two getty entries are added to the end
  of /etc/inittab to support the VPS serial console, and /etc/rc.d/rc.inet1
  is clumsily edited (probably with sed) to change all occurrences of
  'eth' to 'venet', and to add an extra static route. 
  It also turns out that this can be prevented by setting the 'immutable'
  flag on these files.

  * Now you can start the sneaky upgrade from 13.37 to 14.1. 
  Bind-mount the real filesystem root into your new directory:
```
mkdir /new/real
mount --bind / /new/real
```
  * Enter a chroot environment using your new directory:
```
chroot /new
```
  * Move the old directories out of the real root, and copy the new
directories into the real root:
```
mkdir /old
for d in /bin /etc /lib* /sbin /usr /var ; do
  mv /real/$d /old/
  cp -a $d /real/
done
```
  * Exit the chroot. Hopefully you still have a working VPS!
You can now delete /new, and reboot.  Good luck!

#### Slackware 14.2

You can simply install 14.1 as above, and then upgrade manually to 14.2. AlienBOB's
14.1 template has all the required packages and is configured so that
slackpkg will work properly.

The new version of /etc/rc.d/rc.S in 14.2 needs some changes, corresponding to AlienBOB's
rc.S in the 14.1 template.  I've put a copy of my attempt here: [rc.S.new](/files/rc.S.new)

Alternatively, if you are feeling *very* confident, you can create your own
Slackware 14.2 template, using AlienBOB's script
[create_slackware_openvz_template.sh](http://www.slackware.com/~alien/tools/create_slackware_openvz_template.sh)
and Weber K.'s comment from 
[Alien Pastures](http://alien.slackbook.org/blog/running-slackware-14-1-in-an-openvz-vps/)
(see October 10, 2016 at 11:32).
