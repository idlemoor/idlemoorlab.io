---
layout: post
title:  "Video and slides: \"Greetings from SlackBuilds.org\""
date:   2017-07-02 21:00:00 +0000
comments: true
---

<p style="float: left">
<iframe src="https://www.slideshare.net/slideshow/embed_code/key/IRNJyLNHFZAW4h"
  width="300" height="256" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; margin-right:15px; max-width: 100%;" allowfullscreen
  >
</iframe>
</p>

Yesterday I hugely enjoyed a visit to
<a href="http://pkgsrc.org/pkgsrcCon/2017/">pkgsrcCon 2017</a>
in London, where I met
a bunch of awesome people from pkgsrc and the NetBSD community.
Thanks to Sevan for organising a great event.

Here are the slides and the video from a talk I gave about SlackBuilds.org.
Opinions are my own, please don't blame SBo if you think I'm a gobshite.

<p style="clear: both">
<video style="max-width: 100%" controls="controls" preload="none" poster="/files/pkgsrcCon2017_480p.jpg">
  <source type="video/mp4" src="https://files.idlemoor.tk/sbo-at-pkgsrccon.mp4" />
  <source type="video/webm" src="https://files.idlemoor.tk/sbo-at-pkgsrccon.webm" />
</video>
</p>
