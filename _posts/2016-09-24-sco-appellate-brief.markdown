---
layout: post
title:  "SCO v IBM appeal"
date:   2016-09-24 15:00:00 +0100
comments: true
---
(Revised 2017-01-04)

The SCO v IBM appeal is now in progress at the US Court of Appeal Tenth Circuit,
docket no. 16-4040.  

Thanks to <b>smartin_tn</b> and <b>sk43999</b> at [Investor Village](http://www.investorvillage.com/smbd.asp?mb=1911)
for the following documents:
 * [SCO's Opening Brief](/SCO/Appeal-SCO-Opening.pdf) and Appendix (unsealed parts only)
 * [IBM's Reply Brief](/SCO/Appeal-IBM.pdf)
 * [SCO's Reply Brief](/SCO/Appeal-SCO-Reply.pdf)

See the [SCO vs IBM](/SCO/) page for more information.
