---
layout: post
title:  "Getting started with Gitlab Pages"
date:   2016-09-05 23:00:00 +0100
comments: true
---

Here we go with a new blog to record and share detailed technical information
about the stuff I do, both professionally and voluntarily.

But first I'm going to describe what was needed to create this blog.
It's built with <a href="http://jekyllrb.com/">Jekyll</a>, and it's hosted on <a href="https://gitlab.com/">gitlab.com</a>.

#### Setting up Jekyll on Slackware

I started by following the quick start guide at <a href="https://pages.gitlab.io/">https://pages.gitlab.io/</a>, which asks
us to clone <a href="https://gitlab.com/pages/jekyll">https://gitlab.com/pages/jekyll</a> into a Gitlab hosted repo
and do some setup.  This is all quite straightforward.

After that, you can clone the Gitlab repo to your local desktop box. To test and preview
the site locally, a local installation of Jekyll is required.  But I wanted to have proper Slackware
packages, instead of an uncontrolled and unmaintainable mess with gem and bundler.  Jekyll and its dependencies are not available at
<a href="https://slackbuilds.org">SlackBuilds.org</a>, but you can create Slackware packages automatically
from ruby gems with <a href="https://github.com/cycojesus/gem2tgz">gem2tgz</a> by
<a href="https://github.com/cycojesus">cycojesus on Github</a>.  It finds gem
dependencies automatically, and creates a Slackware package for each gem.

gem2tgz is available from SlackBuilds.org, but it's quite old, and I
found that a number of fixups were needed.  You can get a copy from my
forked repo at Github: <a href="https://github.com/idlemoor/gem2tgz">https://github.com/idlemoor/gem2tgz</a>.
When I'm confident that it works nicely, I'll send
cycojesus a pull request :)

Having fixed up gem2tgz, it's as simple as running

```
gem2tgz jekyll
```

and installing the ten packages it creates.

#### Gitlab Pages with a letsencrypt certificate and a custom domain

You've got a lot more hard work to do if you want to use a custom domain and TLS with a certificate from letsencrypt.
There's a tutorial, "<a href="https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt">Securing your GitLab Pages with TLS and Let's Encrypt</a>", but you need to be aware of the following problems:

* On Slackware, don't clone the letsencrypt repo &mdash; you should install the <a href="https://slackbuilds.org/repository/14.2/system/letsencrypt/">letsencrypt SlackBuild</a>
* On Slackware, don't use letsencrypt-auto (you will get the message "Sorry, I don't know how to bootstrap Certbot on your operating system") &mdash; you should use this command:  
  `certbot certonly -a manual -d blog.idlemoor.tk`
* Don't use the name 'lets-encrypt-setup.html' for your custom static page containing the letsencrypt challenge token, and don't include '.html' in the permalink name
  &mdash; you should do what it says in <a href="https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/#comment-2750553052">this comment</a>
* You may need to convert the private key before uploading it &mdash; see <a href="https://gitlab.com/gitlab-com/support-forum/issues/937">this issue</a>
* Certificate and key uploads, including renewal, must be done manually (by deleting and recreating the custom domain) because there is no Gitlab ui or api for changing the certificate and key in an existing domain

#### Random customisations

Then you can customise the template and start blogging.
For example, it's a bit odd that the Gitlab Jekyll template footer includes an icon
for linking to your Github username, but no Gitlab svg icon.  So I made one from the
official svg logo, using inkscape.  You can grab the svg file here:
<a href="https://gitlab.com/idlemoor/idlemoor.gitlab.io/raw/master/_includes/icon-gitlab.svg">icon-gitlab.svg</a>

#### Disqus comments with Jekyll

Finally, I've added Disqus comments. The official
[Disqus configuration for Jekyll](https://help.disqus.com/customer/portal/articles/472138-jekyll-installation-instructions)
works, but I made the following improvements:

[_config.yml](https://gitlab.com/idlemoor/idlemoor.gitlab.io/raw/master/_config.yml)
* Set 'comments' variable off by default
* Set 'disqus_shortname' variable

[_includes/disqus.html](https://gitlab.com/idlemoor/idlemoor.gitlab.io/raw/master/_includes/disqus.html)
* Additional include file for Disqus javascript
* Use 'site.url', 'site.baseurl', 'page.url' and 'site.disqus_shortname' variables in the Disqus javascript
* Wrap the Disqus javascript in a Jekyll footer
