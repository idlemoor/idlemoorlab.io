---
layout: page
title: Slides
permalink: /slides/
---

All these slide decks are licensed
<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC Attribution Share-alike 4.0</a>

<hr style="clear: both; margin: 20px 0 20px 0;" />

<iframe
  src="https://www.slideshare.net/slideshow/embed_code/key/IRNJyLNHFZAW4h"
  width="340" height="290"
  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border: 4px solid #CCC; margin: 0 0 20px 20px; max-width: 100%; float: right"
  allowfullscreen
  >
</iframe>

#### Greetings from SlackBuilds.org

Presented at <a href="http://pkgsrc.org/pkgsrcCon/2017/">pkgsrcCon 2017</a> (July 2017)



<hr style="clear: both; margin: 20px 0 20px 0;" />

<iframe
  src="https://www.slideshare.net/slideshow/embed_code/key/kZqAvQwe5sYgds"
  width="340" height="290"
  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border: 4px solid #CCC; margin: 0 0 20px 20px; max-width: 100%; float: right"
  allowfullscreen
  >
</iframe>

#### Configuration Management and Salt

An introduction to configuration management with Saltstack aka Salt (November 2016)



<hr style="clear: both; margin: 20px 0 20px 0;" />

<iframe
  src="https://www.slideshare.net/slideshow/embed_code/key/1qnXwGPYqCkVx"
  width="340" height="290"
  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border: 4px solid #CCC; margin: 0 0 20px 20px; max-width: 100%; float: right"
  allowfullscreen
  >
</iframe>

#### you won't learn git in twenty minutes at a LUG but here are twenty-odd slides anyway sorry there are no pictures

A non-technical introduction to git (September 2015)



<hr style="clear: both; margin: 20px 0 20px 0;" />

<iframe
  src="https://www.slideshare.net/slideshow/embed_code/key/gYLDGxGbFzsY2W"
  width="340" height="290"
  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border: 4px solid #CCC; margin: 0 0 20px 20px; max-width: 100%; float: right"
  allowfullscreen
  >
</iframe>

#### Slackware Linux: 21 years and still not tried it? What are you waiting for?

In honour of Slackware's 21st anniversary, we dispel some myths and look at 
what is distinctive about today's Slackware (July 2014)



<hr style="clear: both; margin: 20px 0 20px 0;" />

<iframe
  src="https://www.slideshare.net/slideshow/embed_code/key/xODooheNCZ73qq"
  width="340" height="290"
  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"
  style="border: 4px solid #CCC; margin: 0 0 20px 20px; max-width: 100%; float: right"
  allowfullscreen
  >
</iframe>

#### IN UR INTERNETS

What the Snowden revelations are, and why you should care (September 2013)

