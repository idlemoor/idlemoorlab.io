---
layout: page
title: SCO vs IBM
permalink: /SCO/
---
Here are a few pages about the notoriously long-running case of SCO v. IBM,
which is under appeal at the Tenth Circuit of the US Court of Appeal
(docket no. 16-4040). Oral arguments were held on 22 March 2017 before
Circuit Judges [Kelly](https://en.wikipedia.org/wiki/Paul_Joseph_Kelly_Jr.), 
[Ebel](https://en.wikipedia.org/wiki/David_M._Ebel) and 
[Bacharach](https://en.wikipedia.org/wiki/Robert_E._Bacharach).
So now we await judgment to be handed down.
This could, potentially, take many months.

Thanks to smartin_tn and sk43999 for the following documents:
 * [SCO's Opening Brief](Appeal-SCO-Opening.pdf) and Appendix (unsealed parts only)
 * [IBM's Reply Brief](Appeal-IBM.pdf)
 * [SCO's Reply Brief](Appeal-SCO-Reply.pdf)
 * [Docket entry for hearing](Appeal-hearing-docket.pdf)

An interesting feature of IBM's Reply Brief is Appendix A, in which IBM has
made public some significant new extracts of SCO's notorious sealed allegations
against IBM's Linux contributions. Because of this, I am reviving
the page in which I collate and publish the currently known
details of SCO's allegations:

 * [SCO's 294 Alleged Violations](viols.html)

Taken with [The Kevin Trove](trove.html), the new extracts in IBM's brief finally
allow us to see verbatim original pages of each part of this gargantuan secret dossier:

````
  [591] 	22-Dec-2005 	[Sealed] Appendix Volumes I-XX to [589] Disclosure of
  Material Misused by IBM. (Clerk's Note: Appendix volumes are oversized,
  therefore they are not scanned into electronic images for attachment to
  docket event. They are contained in 7 labled boxes. They will be retained in
  the 5th floor sealed room for viewing by the court, and by persons with
  authorization to view by court order only.)
````

We don't have a single complete specimen, but we now know that each
allegation consists of an outline page: for example, here is Item 203:

<a href="Item-203.pdf"><img src="Item-203.png" alt="Item 203" title="Item 203" /></a>

and most Items are supported by "Tabs" of detail: for example, here is Tab 229,
which relates to Item 205:

<a href="Tab-229.pdf"><img src="Tab-229.png" alt="Tab 229" title="Tab229" /></a>

In some cases the Tab is a deposition extract, and in other cases the Tab is "material prepared with the
assistance of several experienced technology consultants". See the
[Declaration of Marc Rochkind, docket no. 669](IBM-669.pdf), Appendix B.

Marc Rochkind, the originator of SCCS, was by his own account the lead consultant
on the Interim and Final Disclosures, but also involved was Gary Nutt of the
University of Colorado Boulder (see [Tab 251](Tab-251.pdf), top left).
We were gratified to learn that many of
[SCO's consultants and experts lost the money they were still owed](http://www.groklaw.net/articlebasic.php?story=20070914152904577)
when SCO went bankrupt in 2007.
