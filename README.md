![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

## the idlemoor technical blog

This is the repository for [idlemoor.gitlab.io](https://idlemoor.gitlab.io)
a.k.a. [blog.idlemoor.tk](https://blog.idlemoor.tk), 
a technical blog about Slackware and SlackBuilds, Open Data, 
Free and Open Source Software, and occasional digressions.
