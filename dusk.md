---
layout: page
title: DUSK
permalink: /dusk/
---

### Dave's Unofficial Slackbuilt Kernels

Every time a new long term support kernel is
released, x86_64 generic and i686 generic-smp packages for Slackware are
automatically built and published within a few hours.

The 4.4 series is intended for use with Slackware 14.2. The configuration of these
kernels is the same as the configuration of the official Slackware 14.2 4.4 series kernels.
  * [Linux 4.4 x86_64 'generic' packages](https://dusk.idlemoor.tk/linux-4.4/x86_64/)
  * [Linux 4.4 i686 'generic-smp' packages](https://dusk.idlemoor.tk/linux-4.4/i686/)

The 4.12 series is intended for use with Slackware current. The configuration of these
kernels is derived from the configuration of the official Slackware current 4.9 series kernels.
  * [Linux 4.12 x86_64 'generic' packages](https://dusk.idlemoor.tk/linux-4.12/x86_64/)
  * [Linux 4.12 i686 'generic-smp' packages](https://dusk.idlemoor.tk/linux-4.12/i686/)

These packages are reproducibly built.  If you want to, you can clone the
['dusk' project from my Gitlab](https://gitlab.com/idlemoor/dusk),
and build your own packages, and your packages should be absolutely identical
to my packages, if you build on the appropriate version of Slackware. And so you
can be assured that I haven't added something evil.

For more information about reproducible building, see
[https://reproducible-builds.org/](https://reproducible-builds.org/)
