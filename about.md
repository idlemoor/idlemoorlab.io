---
layout: page
title: About
permalink: /about/
---

{{ site.description }}

Written by David Spencer &lt;<a href="mailto:{{ site.email }}">{{ site.email }}</a>&gt;  
Public key 2950 AF8E CB51 DF3D
&bull; [keybase.io](https://keybase.io/idlemoor)
&bull; [pgp.mit.edu](http://pgp.mit.edu/pks/lookup?op=get&search=0x2950AF8ECB51DF3D")

 * [gitlab.com/u/idlemoor](https://gitlab.com/u/idlemoor)
 * [github.com/idlemoor](https://github.com/idlemoor)
 &bull; [slackrepo](https://idlemoor.github.io/slackrepo/index.html)
 &bull; [distcc-tools](https://github.com/idlemoor/distcc-tools)
 * [twitter.com/idlemoor](https://twitter.com/idlemoor)
 * [Open Data Services Co-operative](http://opendataservices.coop)
 * [SlackBuilds.org](https://slackbuilds.org)
 * [Bradford Linux Users Group](http://www.bradlug.co.uk)
 * [I.T. Stuff](http://itstuff.org.uk/welcome),
 our monthly programme on [BCB Radio 106.6 FM](http://www.bcbradio.co.uk/)
 and on [Youtube](https://www.youtube.com/channel/UCZNBdwhgMg64D5ohlRLMPyQ)
 * '55020' at [LinuxQuestions Slackware forum](http://www.linuxquestions.org/questions/slackware-14/)
 * 'idlemoor' on [Freenode IRC](http://freenode.net/) #slackbuilds
