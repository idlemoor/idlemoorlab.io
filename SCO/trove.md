---
layout: default
unindexed: yes
title: The Kevin Trove
comments: yes
permalink: /SCO/trove.html
---
<h1 class="post-title">The Kevin Trove</h1>

In July 2010, erstwhile SCO attorney Kevin McBride (Darl McBride's elder
brother) made public a substantial number of source documents that comprised
part of SCO's sealed Final Disclosure. These documents have become known as
<b><i>The Kevin Trove</i></b> and they are preserved here for posterity.

Mr McBride apparently published them without permission, in a fit of pique,
shortly after the adverse verdict in
[SCO v Novell](https://en.wikipedia.org/wiki/SCO_Group,_Inc._v._Novell,_Inc.)
on 10th June 2010.
He believed that these documents prove some tiny parts of Linux and Linux-related
software were copied from Unix SVR4, even if SCO could no longer maintain its
charade of ownership of Unix.

But to anyone skilled in programming, they prove the exact opposite:
  * the program texts have clearly been written independently
  * there are superficial similarities that arise because they are independently
    created embodiments of the same published standard interfaces
  * sometimes SCO's documents shamelessly slice and rearrange the texts to take elements
    out of context and manufacture similarity where none actually exists
    (e.g. [Tab 244](Tab-244.pdf))
  * sometimes the alleged copying is vanishingly small (e.g. [Tab 250](Tab-250.pdf))

Taking Item 217 ([Tab 241](Tab-241.pdf)) as an example, there are 65 lines in
glibc's strings.h, but just two lines -- the prototype declarations of strcasecmp
and strncasecmp -- are complained of, and the only parts of those declarations
that match are the function name and the return type.  Yet the code of those two
functions is not complained of.  If this was theft, the thief would have taken the
diamond tiara instead of the glass case.

The appearance is that Kevin McBride no longer practices law. His
website at [mcbride-law.com](http://mcbride-law.com) displays the message

````
This website is temporarily unavailable, please try again later. 
````
However,
[almost all of The Kevin Trove is preserved at the Internet Archive](https://web.archive.org/web/*/http://www.mcbride-law.com/wp-content/uploads/2010/07/*),
whereby you can verify the provenance of these documents.

| Item 205 | [Tab 229](Tab-229.pdf) | glibc | 2.2.5 | sysdeps/generic/bits/dlfcn.h |
| Item 206 | [Tab 230](Tab-230.pdf) | glibc | 2.2.5 | dlfcn/dlfcn.h |
| Item 207 | [Tab 231](Tab-2311.pdf) | glibc | 2.2.5 | stdlib/fmtmsg.h |
| Item 208 | [Tab 232](Tab-232.pdf) | glibc | 2.2.5 | io/ftw.h |
| Item 209 | [Tab 233](Tab-233.pdf) | linux | 2.4.28 | include/linux/shm.h |
| Item 210 | [Tab 234](Tab-234.pdf) | linux | 2.4.28 | include/linux/ipc.h |
| Item 211 | [Tab 235](Tab-235.pdf) | glibc | 2.2.5 | misc/libgen.h |
| Item 212 | [Tab 236](Tab-236.pdf) | linux | 2.4.28 | include/linux/msg.h |
| Item 213 | [Tab 237](Tab-237.pdf) | glibc | 2.2.5 | sysdep/generic/gits/poll.h |
| Item 214 | [Tab 238](Tab-238.pdf) | linux | 2.4.28 | include/linux/sem.h |
| Item 215 | [Tab 239](Tab-239.pdf) | glibc | 2.2.5 | sysdeps/generic/bits/statvfs.h |
| Item 216 | [Tab 240](Tab-240.pdf) | glibc | 2.2.5 | include/io/sys/statvfs.h |
| Item 217 | [Tab 241](Tab-241.pdf) | glibc | 2.2.5 | string/strings.h |
| Item 218 | [Tab 242](Tab-2421.pdf) | glibc | 2.2.5 | string/string.h |
| Item 219 | [Tab 243](Tab-243.pdf) | glibc | 2.2.5 | streams/stropts.h |
| Item 220 | [Tab 244](Tab-244.pdf) | glibc | 2.2.5 | sysdeps/generic/bits/strops.h |
| Item 221 | [Tab 245](Tab-245.pdf) | glibc | 2.2.5 | sysdeps/generic/bits/shm.h |
| Item 222 | [Tab 246](Tab-2461.pdf) | glibc | 2.2.5 | sysvipc/sys/shm.h |
| Item 223 | [Tab 247](Tab-247.pdf) | glibc | 2.2.5 | misc/sys/syslog.h |
| Item 224 | [Tab 248](Tab-248.pdf) | linux | 2.4.28 | include/asm-i386/ucontext.h |
| Item 225 | [Tab 249](Tab-249.pdf) | linux | 2.4.28 | include/ucontext.h |
| Item 226 | [Tab 250](Tab-250.pdf) | glibc | 2.2.5 | include/ulimit.h |
| Item 227 | [Tab 251](Tab-251.pdf) | glibc | 2.2.5 | resource/ulimit.h |
| Item 228 | [Tab 252](Tab-252.pdf) | linux | 2.4.28 | include/linux/utime.h |
| Item 229 | [Tab 253](Tab-253.pdf) | glibc | 2.2.5 | sysdeps/gnu/utmpx.h |
| Item 230 | [Tab 254](Tab-254.pdf) | glibc | 2.2.5 | gnu/bits/utmpx.h |
| Item 231 | [Tab 255](Tab-255.pdf) | linux | 2.4.28 | include/linux/utsname.h |
| Item 272 | [Tab 329](Tab-329.pdf) | libelf | 0.5.2 | flag.c |
| Item 272 | [Tab 330](Tab-330.pdf) | libelf | 0.5.2 | libelf.h |
| Item 272 | [Tab 331](Tab-331.pdf) | elfutils | 0.97 | libelf.h |
| Item 272 | [Tab 332](Tab-332.pdf) | libelf | 0.5.2 | rand.c |
| Item 272 | [Tab 333](Tab-333.pdf) | libelf | 0.5.2 | strptr.c |
| Item 272 | [Tab 409](Tab-409.pdf) | linux | 2.6.12 | include/linux/elf.h |
| Item 272 | [Tab 410](Tab-410.pdf) | elfutils | 0.97 | elf_fill.c |
| Item 272 | [Tab 411](Tab-411.pdf) | elfutils | 0.97 | elf_getbase.c |
| Item 272 | [Tab 412](Tab-412.pdf) | libelf | 0.5.2 | getehdr.c |
| Item 272 | [Tab 413](Tab-413.pdf) | elfutils | 0.97 | getident.c |
| Item 272 | [Tab 414](Tab-414.pdf) | libelf | 0.5.2 | getphdr.c |
| Item 272 | [Tab 415](Tab-415.pdf) | libelf | 0.5.2 | getscn.c |
| Item 272 | [Tab 416](Tab-416.pdf) | libelf | 0.5.2 | getshdr.c |
| Item 272 | [Tab 417](Tab-417.pdf) | libelf | 0.5.2 | kind.c |
| Item 272 | [Tab 418](Tab-418.pdf) | libelf | 0.5.2 | elf_kind.c |
| Item 272 | [Tab 419](Tab-419.pdf) | libelf | 0.5.2 | ndxscn.c |
| Item 272 | [Tab 420](Tab-420.pdf) | elfutils | 0.97 | elf_ndxscn.c |
| Item 272 | [Tab 421](Tab-421.pdf) | libelf | 0.5.2 | next.c |
| Item 272 | [Tab 422](Tab-422.pdf) | libelf | 0.5.2 | getbase.c |
